デザイナー向けのGit講座 #5

##テーマ: リモートリポジトリを理解2 

##1. プルリク/フォークの確認
* 宿題を確認

##2. originとupstream

##3. タグ機能

##4. submodule

##5. wikiとissue管理

##宿題

###origin と upstreamを理解
*まだフォークしていない人火曜日までにはフォークしておいて下さい

```
upstream: n0bisuke/git.practice
origin: フォークした自分のリポジトリ

upstreamから情報を持ってくる練習なのでフォーク禁止です。

upstreamの内容をpullして最新の情報を取込んでからプルリクエストして下さい。
今回はsiritoriブランチに対してプルリクエストしてみましょう。

変更箇所
lesson5/home_work/README.md

README.mdに記述例が書いています。
このファイル内でしりとりをしてプルリクして下さい。
誰かが答えたら、その内容が最新になるのでプルリク前に常にupstreamを確認して下さい。

プルリク送ったらchatworkで教えてくれるとチェックが早くできます。

マークダウン記法にも慣れましょう。
```
